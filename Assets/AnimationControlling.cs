﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationControlling : MonoBehaviour {
    public bool isMoving;
    public void PlayMoveAnimation(Animator anim)
    {
        anim.SetBool("isMoving", true);
        isMoving = anim.GetBool("isMoving");
    }
    public void PlayIdleAnimation(Animator anim)
    {
        anim.SetBool("isMoving", false);
        isMoving = anim.GetBool("isMoving");
    }
}
